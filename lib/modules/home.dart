import 'package:flutter/material.dart';
import 'package:testmintit/models/user.dart';

class Home extends StatefulWidget {
  const Home({
    Key? key,
  }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Model> person = [
    Model(id: 1, name: 'Max', type: 'Red Bull Racing Honda'),
    Model(id: 2, name: 'Lewis', type: 'Mercedes'),
    Model(id: 3, name: 'Valteri', type: 'Merceds'),
    Model(id: 4, name: 'Sargio', type: 'Red Bull Racing Honda'),
    Model(id: 5, name: 'Carlod', type: 'Ferrari'),
    Model(id: 6, name: 'Lando', type: 'Red Bull Racing Honda'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(children: [
          Container(
            height: 70,
            decoration: const BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.red,
            ),
            child: Row(
              children: [
                const SizedBox(
                  width: 15.0,
                ),
                const CircleAvatar(
                    //backgroundImage: Image.asset('name'),
                    ),
                const SizedBox(
                  width: 15.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text('Good morning',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      Text(
                        'Hayet',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Top 10 ranking 2021',
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
          ),
          const SizedBox(
            height: 20.0,
          ),
          ListView.separated(
            shrinkWrap: true,
            itemBuilder: (context, index) => list(person[index]),
            separatorBuilder: (context, index) => const SizedBox(
              height: 1,
            ),
            itemCount: person.length,
          ),
        ]),
      ),
    );
  }

  Widget list(Model per) => Card(
        elevation: 3,
        child: Row(
          children: [
            const SizedBox(width: 5),
            Text(
              '${per.id}',
              style: const TextStyle(color: Colors.red),
            ),
            const SizedBox(
              width: 40.0,
            ),
            const CircleAvatar(),
            const SizedBox(
              width: 40.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${per.name}',
                    style: const TextStyle(fontWeight: FontWeight.bold)),
                Text(
                  '${per.type}',
                ),
              ],
            ),
          ],
        ),
      );
}
