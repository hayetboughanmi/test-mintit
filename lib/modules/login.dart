import 'package:flutter/material.dart';
import 'package:testmintit/modules/home.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var controllerName = TextEditingController();
  var controllerLastName = TextEditingController();
  late String name;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            //Image.asset('name'),
            const SizedBox(
              height: 150,
            ),
            const Text(
              'Lets Get Familiar',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
            const Text(
              'Introduce Yourself',
              style: TextStyle(
                fontSize: 20,
                color: Colors.grey,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Column(
              children: [
                TextFormField(
                    controller: controllerName,
                    keyboardType: TextInputType.name,
                    decoration: const InputDecoration(
                      labelText: 'your First name',
                      border: OutlineInputBorder(),
                    ),
                    onSaved: (value) {
                      setState(() {
                        name = value!;
                      });
                    }),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: controllerLastName,
                  keyboardType: TextInputType.name,
                  decoration: const InputDecoration(
                    labelText: 'your Last name',
                    border: OutlineInputBorder(),
                  ),
                  onSaved: ((value) => {}),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              height: 40.0,
              width: 100.0,
              child: MaterialButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return Home();
                  }));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      'Next',
                      style: TextStyle(color: Colors.white),
                    ),
                    Icon(
                      Icons.chevron_right,
                      color: Colors.white,
                    ),
                  ],
                ),
                color: Colors.red,
              ),
            )
          ],
        ),
      ),
    );
  }
}
