class Model {
  final int id;
  final String name;
  final String type;

  Model({
    required this.id,
    required this.name,
    required this.type,
  });
}
